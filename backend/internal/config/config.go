package config

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	ApplicationCfg ApplicationCfg `toml:"ApplicationCfg"`
	Database       Database
	Envs           Envs
}

type ApplicationCfg struct {
	JWTTTL int `toml:"JWTTTL"`
}

type Database struct {
	DatabaseHost     string `env:"DATABASE_HOST"`
	DatabaseUser     string `env:"DATABASE_USER"`
	DatabasePassword string `env:"DATABASE_PASSWORD"`
	DatabasePort     string `env:"DATABASE_PORT"`
	DatabaseName     string `env:"DATABASE_NAME"`
}

type Envs struct {
	ServerPort string `env:"SERVER_PORT"`
	JWTKey     string `env:"JWT_KEY"`
}

func New(path string) (*Config, error) {
	cfg := new(Config)

	if err := cleanenv.ReadConfig(path, cfg); err != nil {
		return nil, fmt.Errorf("reading config from %s: %w", path, err)
	}

	return cfg, nil
}
