package auth

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/valyala/fasthttp"
	"time"
)

type Auth struct {
	jwtKey []byte
	jwtttl int
}

func New(jwtKey string, JWTTTL int) (*Auth, error) {
	if jwtKey == "" {
		return nil, fmt.Errorf("empty jwt secret key")
	}

	if JWTTTL <= 0 {
		return nil, fmt.Errorf("invalid jwt ttl")
	}

	return &Auth{
		jwtttl: JWTTTL,
		jwtKey: []byte(jwtKey),
	}, nil
}

type Claims struct {
	Login string `json:"login"`
	jwt.StandardClaims
}

func (a *Auth) GenerateToken(username string) (string, error) {
	expirationTime := time.Now().Add(time.Duration(a.jwtttl) * time.Minute)
	claims := &Claims{
		Login: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(a.jwtKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (a *Auth) AuthorizationMiddleware(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		authHeader := string(ctx.Request.Header.Peek("Authorization"))
		if authHeader == "" {
			ctx.Error("missing auth header", fasthttp.StatusUnauthorized)
			return
		}

		tokenString := authHeader[len("Bearer "):]
		claims := new(Claims)

		token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return a.jwtKey, nil
		})
		if err != nil || !token.Valid {
			ctx.Error("invalid auth header", fasthttp.StatusUnauthorized)
			return
		}

		next(ctx)
	}
}
