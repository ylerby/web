package handler

import (
	"backend/internal/config"
	"backend/internal/domain"
	"backend/internal/model"
	"encoding/json"
	"fmt"
	"github.com/apsdehal/go-logger"
	"github.com/valyala/fasthttp"
)

type repository interface {
	Login(loginFields domain.LoginFields) (string, error)
	Register(registerFields domain.RegisterFields) error
	Create(createFields domain.CreateFields) error
	Read() ([]model.Animal, error)
	Update(updateFields domain.UpdateFields) error
	Delete(id int) error
	ReadByClass(className string) ([]model.Animal, error)
	ReadBySize(readBySizeFields domain.ReadBySizeFields) ([]model.Animal, error)
	ReadByArea(area string) ([]model.Animal, error)
}

type Handler struct {
	r repository

	l   *logger.Logger
	cfg *config.Config
}

func New(r repository, l *logger.Logger, cfg *config.Config) *Handler {
	return &Handler{
		r: r,

		l:   l,
		cfg: cfg,
	}
}

func (h *Handler) PanicHandler(ctx *fasthttp.RequestCtx, _ interface{}) {
	ctx.Error("panic occurred", fasthttp.StatusInternalServerError)
}

func (h *Handler) NotFoundHandler(ctx *fasthttp.RequestCtx) {
	ctx.Error("not found", fasthttp.StatusNotFound)
}

func (h *Handler) MethodNotAllowedHandler(ctx *fasthttp.RequestCtx) {
	ctx.Error("method not allowed", fasthttp.StatusMethodNotAllowed)
}

func (h *Handler) Login(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.LoginFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	token, err := h.r.Login(*reqBody)
	if err != nil {
		h.l.Errorf("login error: %v", err.Error())
		ctx.Error(fmt.Sprintf("login error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	loginResponseBody := domain.LoginResponseBody{
		Token: token,
	}

	respBody, err := json.Marshal(&loginResponseBody)
	if err != nil {
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) Register(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.RegisterFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.Register(*reqBody)
	if err != nil {
		h.l.Errorf("register error: %v", err.Error())
		ctx.Error(fmt.Sprintf("register error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) Create(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.CreateFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.Create(*reqBody)
	if err != nil {
		h.l.Errorf("creating error: %v", err.Error())
		ctx.Error(fmt.Sprintf("creating error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) Read(ctx *fasthttp.RequestCtx) {
	records, err := h.r.Read()
	if err != nil {
		h.l.Errorf("reading error: %v", err.Error())
		ctx.Error(fmt.Sprintf("reading error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	respBody, err := json.Marshal(records)
	if err != nil {
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) Update(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.UpdateFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	if id := reqBody.ID; id < 0 {
		ctx.Error("invalid id value", fasthttp.StatusBadRequest)

		return
	}

	err = h.r.Update(*reqBody)
	if err != nil {
		h.l.Errorf("updating error: %v", err.Error())
		ctx.Error(fmt.Sprintf("updating error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) Delete(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.DeleteFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	id := reqBody.ID
	if id < 0 {
		ctx.Error("invalid id value", fasthttp.StatusBadRequest)

		return
	}

	err = h.r.Delete(id)
	if err != nil {
		h.l.Errorf("deleting error: %v", err.Error())
		ctx.Error(fmt.Sprintf("deleting error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) ReadByClass(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.ReadByClassFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	class := reqBody.ClassName
	if class == "" {
		ctx.Error("empty class field", fasthttp.StatusBadRequest)

		return
	}

	records, err := h.r.ReadByClass(reqBody.ClassName)
	if err != nil {
		h.l.Errorf("reading by class error: %v", err.Error())
		ctx.Error(fmt.Sprintf("reading by class error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	respBody, err := json.Marshal(records)
	if err != nil {
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) ReadBySize(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.ReadBySizeFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ll := reqBody.LowerSizeLimit
	ul := reqBody.UpperSizeLimit

	if ll <= 0 || ul <= 0 {
		ctx.Error("invalid size limit", fasthttp.StatusBadRequest)

		return
	}

	records, err := h.r.ReadBySize(*reqBody)
	if err != nil {
		h.l.Errorf("reading by size error: %v", err.Error())
		ctx.Error(fmt.Sprintf("reading by size error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	respBody, err := json.Marshal(records)
	if err != nil {
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) ReadByArea(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.ReadByAreaFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	area := reqBody.Area
	if area == "" {
		ctx.Error("empty area field", fasthttp.StatusBadRequest)

		return
	}

	records, err := h.r.ReadByArea(area)
	if err != nil {
		h.l.Errorf("reading by area error: %v", err.Error())
		ctx.Error(fmt.Sprintf("reading by area error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	respBody, err := json.Marshal(records)
	if err != nil {
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}
