package server

import (
	"backend/internal/handler"
	"backend/internal/service/auth"
	"github.com/buaazp/fasthttprouter"
)

func New(
	handler *handler.Handler,
	auth *auth.Auth,
) *fasthttprouter.Router {
	router := &fasthttprouter.Router{
		RedirectTrailingSlash:  true,
		RedirectFixedPath:      true,
		HandleOPTIONS:          true,
		HandleMethodNotAllowed: true,

		PanicHandler:     handler.PanicHandler,
		NotFound:         handler.NotFoundHandler,
		MethodNotAllowed: handler.MethodNotAllowedHandler,
	}

	router.POST("/api/v1/login", handler.Login)
	router.POST("/api/v1/register", handler.Register)

	router.POST("/api/v1/create", auth.AuthorizationMiddleware(handler.Create))
	router.GET("/api/v1/read", auth.AuthorizationMiddleware(handler.Read))
	router.PUT("/api/v1/update", auth.AuthorizationMiddleware(handler.Update))
	router.DELETE("/api/v1/delete", auth.AuthorizationMiddleware(handler.Delete))

	router.POST("/api/v1/read/class", auth.AuthorizationMiddleware(handler.ReadByClass))
	router.POST("/api/v1/read/size", auth.AuthorizationMiddleware(handler.ReadBySize))
	router.POST("/api/v1/read/area", auth.AuthorizationMiddleware(handler.ReadByArea))

	return router
}
