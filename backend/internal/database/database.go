package database

import (
	"backend/internal/domain"
	"backend/internal/model"
	"errors"
	"fmt"
	"gorm.io/gorm"
)

var Classes = map[string]int{
	"планктон":        1,
	"медузы и полипы": 2,
	"моллюски":        3,
	"хордовые":        4,
}

func (d *Database) Login(login string) (*model.User, error) {
	record := new(model.User)
	result := d.db.Where("login = ?", login).Find(record)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return nil, fmt.Errorf("record not found")
	}

	return record, nil
}

func (d *Database) Register(login, password string) error {
	record := &model.User{
		Login:    login,
		Password: password,
	}

	result := d.db.Create(record)

	err := result.Error

	switch {
	case errors.Is(err, gorm.ErrDuplicatedKey):
		return fmt.Errorf("user already exists")
	case err != nil:
		return fmt.Errorf("db error occurred: %w", err)
	}

	return nil
}

func (d *Database) Create(createFields domain.CreateFields) error {
	animal := model.Animal{
		Name:           createFields.Name,
		Description:    createFields.Description,
		Size:           createFields.Size,
		LifeExpectancy: createFields.LifeExpectancy,
		Area:           createFields.Area,
		Class:          Classes[createFields.ClassName],
	}

	createResult := d.db.Create(&animal)

	if err := createResult.Error; err != nil {
		return fmt.Errorf("db (insert) error occurred: %w", err)
	}

	return nil
}

func (d *Database) Read() ([]model.Animal, error) {
	records := make([]model.Animal, 0)

	result := d.db.Find(&records)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db (select animal) error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return []model.Animal{}, nil
	}

	return records, nil
}

func (d *Database) Update(updateFields domain.UpdateFields) error {
	var class model.Class
	result := d.db.Where("name = ?", updateFields.ClassName).First(&class)

	if err := result.Error; err != nil {
		return fmt.Errorf("db (select) error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("class not found")
	}

	var animal model.Animal

	var updatedData = map[string]interface{}{
		"name":            updateFields.Name,
		"description":     updateFields.Description,
		"size":            updateFields.Size,
		"life_expectancy": updateFields.LifeExpectancy,
		"area":            updateFields.Area,
		"class_id":        class.ClassID,
	}

	result = d.db.Where("id = ?", updateFields.ID).Model(&animal).Updates(updatedData)

	if err := result.Error; err != nil {
		return fmt.Errorf("db (update) error occurred")
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("not found")
	}

	return nil
}

func (d *Database) Delete(id int) error {
	var animal model.Animal

	result := d.db.Where("id = ?", id).Delete(&animal)

	if err := result.Error; err != nil {
		return fmt.Errorf("db error occurred")
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("not found")
	}

	return nil
}

func (d *Database) ReadByClass(className string) ([]model.Animal, error) {
	var class model.Class
	result := d.db.Where("name = ?", className).First(&class)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db (select class) error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return nil, fmt.Errorf("class not found")
	}

	records := make([]model.Animal, 0)

	result = d.db.Where("class_id = ?", class.ClassID).Find(&records)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db (select animals) error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return []model.Animal{}, nil
	}

	return records, nil
}

func (d *Database) ReadBySize(lowerLimit, upperLimit float64) ([]model.Animal, error) {
	records := make([]model.Animal, 0)

	result := d.db.Where("size between ? and ?", lowerLimit, upperLimit).
		Find(&records)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return []model.Animal{}, nil
	}

	return records, nil
}

func (d *Database) ReadByArea(area string) ([]model.Animal, error) {
	records := make([]model.Animal, 0)

	result := d.db.Where("area = ?", area).Find(&records)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return []model.Animal{}, nil
	}

	return records, nil
}
