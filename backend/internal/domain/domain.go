package domain

type LoginFields struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type LoginResponseBody struct {
	Token string `json:"token"`
}

type RegisterFields struct {
	Login         string `json:"login"`
	Password      string `json:"password"`
	AgainPassword string `json:"again-password"`
}

type CreateFields struct {
	Name           string  `json:"name"`
	Description    string  `json:"description"`
	Size           float64 `json:"size"`
	LifeExpectancy uint8   `json:"life-expectancy"`
	Area           string  `json:"area"`
	ClassName      string  `json:"class-name"`
}

type UpdateFields struct {
	ID             int     `json:"id"`
	Name           string  `json:"name"`
	Description    string  `json:"description"`
	Size           float64 `json:"size"`
	LifeExpectancy uint8   `json:"life-expectancy"`
	Area           string  `json:"area"`
	ClassName      string  `json:"class-name"`
}

type DeleteFields struct {
	ID int `json:"id"`
}

type ReadByClassFields struct {
	ClassName string `json:"class-name"`
}

type ReadBySizeFields struct {
	LowerSizeLimit float64 `json:"lower-size-limit"`
	UpperSizeLimit float64 `json:"upper-size-limit"`
}

type ReadByAreaFields struct {
	Area string `json:"Area"`
}
