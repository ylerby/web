package middleware

import (
	"github.com/apsdehal/go-logger"
	"github.com/valyala/fasthttp"
	"time"
)

func Cors(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
		ctx.Response.Header.Set("Access-Control-Allow-Methods", "*")
		ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")

		next(ctx)
	}
}

func Logging(next fasthttp.RequestHandler, logger *logger.Logger) fasthttp.RequestHandler {
	location, _ := time.LoadLocation("Europe/Moscow")
	return func(ctx *fasthttp.RequestCtx) {
		logger.Noticef("request at: %s, to: %s",
			time.Now().In(location).Format(time.TimeOnly),
			string(ctx.Request.URI().Path()),
		)

		next(ctx)
	}
}
