package model

type User struct {
	Login    string `gorm:"column:login;primaryKey;varchar(64)"`
	Password string `gorm:"column:password;varchar(64)"`
}

type Animal struct {
	ID             int     `gorm:"column:id;primaryKey;autoIncrement"`
	Name           string  `gorm:"column:name;type:varchar(64)"`
	Description    string  `gorm:"column:description;type:varchar(255)"`
	Size           float64 `gorm:"column:size;type:float"`
	LifeExpectancy uint8   `gorm:"column:life_expectancy;type:integer"`
	Area           string  `gorm:"column:area;type:varchar(64)"`
	Class          int     `gorm:"column:class_id"`
}

type Class struct {
	ClassID int    `gorm:"column:class_id;primaryKey;autoIncrement"`
	Name    string `gorm:"column:name;type:varchar(64)"`
}
