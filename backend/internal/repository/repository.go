package repository

import (
	"backend/internal/database"
	"backend/internal/domain"
	"backend/internal/model"
	"backend/internal/service/auth"
	"backend/internal/service/encryption"
	"errors"
	"fmt"
)

type Repository struct {
	db   *database.Database
	auth *auth.Auth
}

func New(opts ...Option) (*Repository, error) {
	repository := new(Repository)

	errs := make([]error, 0)

	for _, o := range opts {
		if err := o(repository); err != nil {
			errs = append(errs, err)
		}
	}

	return repository, errors.Join(errs...)
}

func (r *Repository) Login(loginFields domain.LoginFields) (string, error) {
	if loginFields.Password == "" {
		return "", fmt.Errorf("empty password")
	}

	if loginFields.Login == "" {
		return "", fmt.Errorf("empty login")
	}

	user, err := r.db.Login(loginFields.Login)
	if err != nil {
		return "", fmt.Errorf("user info getting: %w", err)
	}

	err = encryption.CompareHashAndPassword(loginFields.Password, user.Password)
	if err != nil {
		return "", fmt.Errorf("matching: %w", err)
	}

	token, err := r.auth.GenerateToken(loginFields.Login)
	if err != nil {
		return "", fmt.Errorf("token generating: %w", err)
	}

	return token, nil
}

func (r *Repository) Register(registerFields domain.RegisterFields) error {
	if registerFields.Password == "" {
		return fmt.Errorf("empty password")
	}

	if registerFields.Login == "" {
		return fmt.Errorf("empty login")
	}

	if registerFields.Password != registerFields.AgainPassword {
		return fmt.Errorf("passwords mismatching")
	}

	hashedPassword, err := encryption.HashPassword(registerFields.Password)
	if err != nil {
		return fmt.Errorf("password encryption: %w", err)
	}

	err = r.db.Register(registerFields.Login, hashedPassword)
	if err != nil {
		return fmt.Errorf("user creating: %w", err)
	}

	return nil
}

func (r *Repository) Create(createFields domain.CreateFields) error {
	err := r.db.Create(createFields)
	if err != nil {
		return fmt.Errorf("creating: %w", err)
	}

	return nil
}

func (r *Repository) Read() ([]model.Animal, error) {
	records, err := r.db.Read()
	if err != nil {
		return nil, fmt.Errorf("reading: %w", err)
	}

	return records, nil
}

func (r *Repository) Update(updateFields domain.UpdateFields) error {
	err := r.db.Update(updateFields)
	if err != nil {
		return fmt.Errorf("updating: %w", err)
	}

	return nil
}

func (r *Repository) Delete(id int) error {
	err := r.db.Delete(id)
	if err != nil {
		return fmt.Errorf("deleting: %w", err)
	}

	return nil
}

func (r *Repository) ReadByClass(className string) ([]model.Animal, error) {
	records, err := r.db.ReadByClass(className)
	if err != nil {
		return nil, fmt.Errorf("reading by class: %w", err)
	}

	return records, nil
}

func (r *Repository) ReadBySize(readBySizeFields domain.ReadBySizeFields) ([]model.Animal, error) {
	records, err := r.db.ReadBySize(
		readBySizeFields.LowerSizeLimit,
		readBySizeFields.UpperSizeLimit,
	)
	if err != nil {
		return nil, fmt.Errorf("reading by size: %w", err)
	}

	return records, nil
}

func (r *Repository) ReadByArea(area string) ([]model.Animal, error) {
	records, err := r.db.ReadByArea(area)
	if err != nil {
		return nil, fmt.Errorf("reading by area: %w", err)
	}

	return records, nil
}
