package main

import (
	"backend/internal/config"
	"context"
	"flag"
	"github.com/valyala/fasthttp"
	"log"
	"os"
	"os/signal"
	"syscall"

	webHandler "backend/internal/handler"
	webRepository "backend/internal/repository"
	webServer "backend/internal/server"
	webAuth "backend/internal/service/auth"
	webLogger "github.com/apsdehal/go-logger"
)

func main() {
	cfgFilePath := flag.String("cfgfilepath", "configs/config.toml", "config.toml file path")
	flag.Parse()

	logger, err := webLogger.New("web-backend", 1, os.Stdout)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	cfg, err := config.New(*cfgFilePath)
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	logger.Noticef("cfg: %v", cfg)

	auth, err := webAuth.New(cfg.Envs.JWTKey, cfg.ApplicationCfg.JWTTTL)
	if err != nil {
		logger.Fatalf("failed to init auth: %v", err)
	}

	repository, err := webRepository.New(
		webRepository.WithDatabase(cfg.Database),
		webRepository.WithAuth(auth),
	)
	if err != nil {
		logger.Fatalf("failed to init repository: %v", err)
	}

	handler := webHandler.New(
		repository,
		logger,
		cfg,
	)

	server := webServer.New(
		handler,
		auth,
	)

	go func() {
		err = fasthttp.ListenAndServe(":"+cfg.Envs.ServerPort, server.Handler)
		if err != nil {
			logger.Errorf("failed to run http server: %v", err)
		}
	}()

	logger.Infof("starting API listening at :%s", cfg.Envs.ServerPort)

	<-ctx.Done()
}
